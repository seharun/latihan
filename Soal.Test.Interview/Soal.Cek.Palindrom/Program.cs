﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soal.Cek.Palindrom
{
    class Program
    {
        public static bool CekPalindrome(string inputstr)
        {
             var reversestr = string.Empty;

            for (int i = inputstr.Length - 1; i >= 0; i--)
            {
                reversestr += inputstr[i].ToString();
            }
            if (reversestr == inputstr)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static void Main(string[] args)
        {
            string inputstr = string.Empty;
            Console.Write("Enter a string : ");
            inputstr = Console.ReadLine();
            if (inputstr != null)
            {
                if (CekPalindrome(inputstr))
                {
                    Console.WriteLine("String is Palindrome");
                }
                else
                {
                    Console.WriteLine("String is not Palindrome");
                }
            }
            Console.ReadLine();
        }
    }
}
