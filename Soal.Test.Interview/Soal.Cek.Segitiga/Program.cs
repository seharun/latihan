﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soal.Cek.Segitiga
{
    public class Program
    {
        public enum Type
        {
            SAMA_KAKI, SAMA_SISI, IRREGULAR, BUKAN_SEGITIGA
        };

        public static Type triangleType(int a, int b, int c)
        {
            var type = Type.BUKAN_SEGITIGA;
            if (new[] { a, b, c }.Distinct().Count() == 1)
            {
                type = Type.SAMA_SISI;
            }
            else if (new[] { a, b }.Distinct().Count() == 1)
            {
                type = Type.SAMA_KAKI;
            }
            else if (new[] { a, b, c }.Distinct().Count() == 3)
            {
                type = Type.IRREGULAR;
            }


            return type;
        }

        static void Main(string[] args)
        {
            var result = triangleType(5,7,8);
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
