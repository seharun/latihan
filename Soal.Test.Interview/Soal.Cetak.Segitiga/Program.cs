﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soal.Cetak.Segitiga
{
    class Program
    {
        public static void Process(int angka)
        {
            for (int i = 1; i <= angka; i++)
            {
                for (int j = i; j <= 2 * i - 1; j++)
                {
                    Console.Write("*");
                    Console.Write(" ");
                }
                Console.WriteLine();
                
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Enter angka : ");
            var input = Console.ReadLine();
            var inputInt = Convert.ToInt32(input);
            Process(inputInt);
            Console.ReadLine();
        }
    }
}
